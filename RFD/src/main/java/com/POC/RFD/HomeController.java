package com.POC.RFD;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import net.sf.json.JSONObject;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@RequestMapping(value = "/")
	public void home(HttpServletRequest request, HttpServletResponse Response) throws IOException {
		
		JSONObject json = new JSONObject();
		
		json.put("Callback", request.getParameter("callback"));
		
		Response.setContentType("application/json");
		//Response.setHeader("Content-disposition", "attachment");
		Response.getWriter().println(json.toString());
	}
	
	@RequestMapping(value = "/Reply")
	public ModelAndView Reply(HttpServletRequest request, HttpServletResponse Response) throws IOException {
		ModelAndView mav = new ModelAndView("Reply");
		
		return mav;
	}
}
